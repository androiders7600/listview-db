package com.example.listview.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.hardware.Camera.Size;
import android.util.Log;


public class DataBase extends SQLiteOpenHelper{
	
	
	private static final String TABLE_NAME = "person_table";         // Table name
	private static final String PERSON_TABLE_COLUMN_ID = "_id";     // a column named "_id" is required for cursor
	private static final String PERSON_TABLE_COLUMN_NAME = "person_name";
	private static final String PERSON_TABLE_COLUMN_PIN = "person_pin";

	//private DatabaseOpenHelper openHelper;
	public SQLiteDatabase database;
	

	public DataBase(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}
	

	public DataBase(Context context) {
		super(context, "DATABASE_NAME", null, 1);
	}
	
	
	
	public void insertData (String aPersonName, String aPersonPin, String aPersonID) {
		database = this.getWritableDatabase();

	    // we are using ContentValues to avoid sql format errors

	    ContentValues contentValues = new ContentValues();

	    contentValues.put(getPersonTableColumnName(), aPersonName);
	    contentValues.put(PERSON_TABLE_COLUMN_PIN, aPersonPin);
	    contentValues.put(PERSON_TABLE_COLUMN_ID, aPersonID);

	    database.insert(TABLE_NAME, null, contentValues);
	}
	
	public  Cursor getAllData () {
		//database = this.getReadableDatabase();
		
		
		initialiseDB();

	    String buildSQL = "SELECT * FROM " + TABLE_NAME;

	    Log.d("TAG", "getAllData SQL: " + buildSQL);

	    return database.rawQuery(buildSQL, null);
	}
	
	
	public Cursor getTenRows(int start, int stop){
		database = this.getReadableDatabase();
		String buildSQL = "SELECT * FROM "+ TABLE_NAME+" order by "+ PERSON_TABLE_COLUMN_ID+ " limit 11";//"SELECT * FROM "+ TABLE_NAME + " WHERE "+ PERSON_TABLE_COLUMN_NAME + " BETWEEN ? AND ?";
		 return database.rawQuery(buildSQL, null);
	}

	

	
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
		String buildSQL = "CREATE TABLE " + TABLE_NAME + "( " + PERSON_TABLE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                getPersonTableColumnName() + " TEXT, " + PERSON_TABLE_COLUMN_PIN + " TEXT )";
		
		  Log.d("TAG", "onCreate SQL: " + buildSQL);
		 
	      db.execSQL(buildSQL);
	      
		
	}
	
	public void  initialiseDB(){
		
		
//	    insertData("","","0");
//		insertData("Person A", "PIN001", "1");
//		insertData("Person B", "PIN002", "2");
//		insertData("Person C", "PIN003","3");
//		insertData("Person D", "PIN004", "4");
		
		for(int i=0; i<=100;i++){
			insertData("Person "+ String.valueOf(i), "Pin"+ String.valueOf(i), String.valueOf(i));
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
		 String buildSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;
	        Log.d("TAG", "onUpgrade SQL: " + buildSQL);
	        db.execSQL(buildSQL);       // drop previous table
	        onCreate(db);               // create the table from the beginning		
	}

	
	




	public static String getPersonTableColumnName() {
		return PERSON_TABLE_COLUMN_NAME;
	}



	
	

}
