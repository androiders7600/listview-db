package com.example.listview;

import java.util.ArrayList;

import com.example.listview.database.DataBase;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.view.Menu;
import android.view.View;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ListActivity {
	
	public Cursor cursorNames;
	private String[] lv_arr;
	DataBase db = new DataBase(this);
	ArrayAdapter<String> adapter;
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final ListView listview = getListView();

		CursorFactory cursorfactory = new CursorFactory() {
        	@Override
			public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver masterQuery,
					String editTable, SQLiteQuery query) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		listview.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "ITEM CLICKED", Toast.LENGTH_LONG).show();		
			}
		});
		
		
		
	   // DataBase db = new DataBase(getApplicationContext(), "Sample",  cursorfactory, 1);
		
        cursorNames = db.getAllData();

		int value =cursorNames.getCount();
//        cursorNames.close();
		
		
		String text = String.valueOf(value);
		
		ArrayList<String> columnArray1 = new ArrayList<String>();
		ArrayList<String> columnArray2 = new ArrayList<String>();
		
		cursorNames.moveToFirst();
		while (cursorNames.moveToNext()) {
		    // Your code
			columnArray1.add(cursorNames.getString(1));
		    columnArray2.add(cursorNames.getString(2));
		}
		
		
		
//		for(cursorNames.moveToFirst(); cursorNames.moveToNext(); cursorNames.isAfterLast()) {
//		//for(int i=0;i<cursorNames.getString(1).length() ;i++){
//		    columnArray1.add(cursorNames.getString(1));
//		    columnArray2.add(cursorNames.getString(2));
//		}
		
		String[] stockArr = new String[columnArray1.size()];
		stockArr = columnArray1.toArray(stockArr);
		
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, stockArr);
		listview.setAdapter(adapter);
		value = 0;
		
		
		cursorNames.close();
		
		Button chooseButton = (Button) findViewById(R.id.ChooseButton);
		chooseButton.setOnClickListener( new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Cursor cursor10Elements = db.getTenRows(0, 10);
				ArrayList<String> column10Elements = new ArrayList<String>();
				cursor10Elements.moveToFirst();
				while (cursor10Elements.moveToNext()) {
				    // Your code
					column10Elements.add(cursor10Elements.getString(1));
				}
					String[] stockArr = new String[column10Elements.size()];
					stockArr = column10Elements.toArray(stockArr);
					
					//ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, stockArr);
					ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,android.R.id.text1 , stockArr);
                if(stockArr.length!=0)
				Toast.makeText(getApplicationContext(), stockArr[2], Toast.LENGTH_LONG).show();
                else
                	Toast.makeText(getApplicationContext(), "Null", Toast.LENGTH_LONG).show();
				
				listview.setAdapter(null);
				
				listview.setAdapter(adapter1);
			}
			
			
		});
		
		
		
		
		
		
		
		
		
	}
	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
